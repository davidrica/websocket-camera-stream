# websocket-camera-stream

## Table of Contents

- [Getting Started](#getting_started)
- [source](#source)

## Getting Started

These instructions will get you a copy of the project up and running on your local machine and connected to port forward VPN.



### Installing


```
npm install or yarn install
```

Run Websocket Server


```
node websocket-relay.js supersecret 8081 8291
```

Run Static Web Server


```
sudo http-server -p 80
```

Stream Web Cam


```
ffmpeg \
	-f v4l2 \
		-framerate 25 -video_size 640x480 -i /dev/video0 \
	-f mpegts \
		-codec:v mpeg1video -s 640x480 -b:v 1000k -bf 0 \
	http://localhost:8081/supersecret
```

Open live stream camera url from browser


```
http://id11.tunnel.my.id:1366/view-stream.html
```

Enjoy.

## Source

https://github.com/phoboslab/jsmpeg
