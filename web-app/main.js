var canvas = document.getElementById('video-canvas');

var connection = io( 'http://192.168.100.26:8291' );
let connectionId = null;

// when connection is established 
connection.on( 'connect', (data) => {
  // connectionId = connection.io.engine.id;
  isConnectionActive = true;
  var eio = connection.io.engine.transport.query.EIO;
  var connectionId = connection.io.engine.id;
  var url = `ws://192.168.100.26:8291/socket.io/?EIO=${eio}&transport=websocket&sid=${connectionId}`;
  var player = new JSMpeg.Player(url, {canvas: canvas});
} );

connection.on( 'disconnect', () => {
  isConnectionActive = false;
} );

connection.on('led-toggle', function (data) { //get pin status from server
  console.log('dataToggle: ', data);
});

// WebSocket event emitter function
var emitEvent = function( event ) {
  if( ! isConnectionActive ) {
    return alert( 'Server connection is closed!' );
  }

  // emit `led-toggle` socket event
  // connection.emit( 'led-toggle', {
  //   // r: button_red_state,
  //   // g: button_green_state,
  //   // b: button_blue_state,
  // } );
};

// add event listeners on button
// button_red.addEventListener( 'click', emitEvent );
// button_blue.addEventListener( 'click', emitEvent );
// button_green.addEventListener( 'click', emitEvent );