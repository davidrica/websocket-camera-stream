
// import `onoff` package
const { Gpio } = require( 'onoff' );

// configure LED pins
const pin_red = new Gpio( 26, 'out' );
const pin_green = new Gpio( 19, 'out' );
const pin_blue = new Gpio( 13, 'out' );

let pinState = {
  red: false,
  green: false,
  blue: false,
};


// toggle LED states
exports.toggle = ( client, r, g, b ) => {
  if (pinState.red != r) {
    pinState.red = r;
    pin_red.write( r ? 1 : 0, err => {
      if (err) {throw err;}
      client.emit('led-toggle', pinState);
    });
  }
  if (pinState.green != g) {
    pinState.green = g;
    pin_green.write( g ? 1 : 0, err => {
      if (err) {throw err;}
      client.emit('led-toggle', pinState);
    });
  }
  if (pinState.blue != b) {
    pinState.blue = b;
    pin_blue.write( b ? 1 : 0, err => {
      if (err) {throw err;}
      client.emit('led-toggle', pinState);
    });
  }
  
};

// watch LED states
exports.getInitialState = (client) => {
  let pinState = {
    red: false,
    green: false,
    blue: false,
  };

  pin_red.read(function (err, value) { //Read for pin state
    if (err) { //if an error
      console.error('There was an error', err); //output error message to console
      return;
    }
    pinState.red = value ? true : false;
  });

  pin_green.read(function (err, value) {
    if (err) {
      console.error('There was an error', err);
      return;
    }
    pinState.green = value ? true : false;
  });

  pin_blue.read(function (err, value) {
    if (err) {
      console.error('There was an error', err);
      return;
    }
    pinState.blue = value ? true : false;
    client.emit('led-toggle', pinState); //send initial GPIO status to client
  });
  
}